import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('token') || null,
        authData: JSON.parse(localStorage.getItem('authdata')) || null,
        authload: false,
        chatTarget: null,
        newMessage: false,
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.authload,
        userLoggedIn: state => state.authData,
        chatTarget: state => state.chatTarget,
        newMessage: state => state.newMessage,
    },
    mutations: {
        AUTH_REQUEST: (state) => {
            state.authload = true
        },
        AUTH_SUCCESS: (state, userData) => {
            state.authload = false
            state.token = userData.user.token
            state.authData = userData
        },
        AUTH_ERROR: (state) => {
            state.authload = false
        },
        chatTarget: (state, chatTarget) => {
            state.chatTarget = chatTarget
        },
        newMessage: (state) => {
            state.newMessage = true
        }
    },   
    actions: {
        AUTH_REQUEST: async ({commit}, userData) => {
            return new Promise((resolve) => {
              commit('AUTH_REQUEST');
              commit('AUTH_SUCCESS', userData)
              resolve();
            })
        },
        chatTarget: async ({commit}, chatTarget) => {
            return new Promise((resolve) => {
                commit('chatTarget', chatTarget);
                resolve();
            })
        },
        newMessage: async ({commit}) => {
            return new Promise((resolve) => {
                commit('newMessage');
                resolve();
            })
        },
    },
    modules: '',
})