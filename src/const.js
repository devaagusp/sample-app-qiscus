import QiscusSDKCore from 'qiscus-sdk-core'
const qiscus = new QiscusSDKCore()
qiscus.init({
  AppId: 'sdksample',
  options: {
    newMessagesCallback(messages) {
      var message = messages[0]
      // Do something with message
      console.log('New message', message)
    }
  }
})

export default qiscus;