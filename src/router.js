import Vue from 'vue';
import Router from 'vue-router';
import Login from './components/LoginPage.vue';
import ChatBox from './components/ChatBox.vue';
import ChatRoom from './components/ChatRoom.vue';

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: Login,
        }, 
        {
            path: '/',
            name: 'ChatBox',
            component: ChatBox,
            props: true,
            meta: {
                requiresAuth: true
            },
        },
        {
            path: '/chat',
            name: 'ChatRoom',
            component: ChatRoom,
            props: true,
            meta: {
                requiresAuth: true
            },
        }
    ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(value => value.meta.requiresAuth)) {
    if (localStorage.getItem('token') == null) {
      next({
        path: '/login',
      });
    } else {
      next();
    }
  } else if (to.path === '/login' && localStorage.getItem('token') != null) {
    next({
      path: from.path,
      replace: true,
    });
  } else {
    next();
  }
});

export default router;